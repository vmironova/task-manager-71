package ru.t1consulting.vmironova.tm.enumerated;

public enum RoleType {

    ADMINISTRATOR,
    USER;

}
