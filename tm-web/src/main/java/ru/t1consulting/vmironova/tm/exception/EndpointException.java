package ru.t1consulting.vmironova.tm.exception;

import org.jetbrains.annotations.NotNull;

public final class EndpointException extends AbstractException {

    public EndpointException(@NotNull String message) {
        super(message);
    }

}
