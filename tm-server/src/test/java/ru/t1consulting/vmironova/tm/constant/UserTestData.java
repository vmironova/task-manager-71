package ru.t1consulting.vmironova.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.PropertySource;
import ru.t1consulting.vmironova.tm.dto.model.UserDTO;
import ru.t1consulting.vmironova.tm.util.HashUtil;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

@UtilityClass
@PropertySource("classpath:application.properties")
public final class UserTestData {

    @NotNull
    public final static String USER_TEST_LOGIN = "USER_TEST_LOGIN";

    @NotNull
    public final static String USER_TEST_PASSWORD = "USER_TEST_PASSWORD";

    @NotNull
    public final static String USER_TEST_EMAIL = "USER_TEST@EMAIL";

    @NotNull
    public final static String ADMIN_TEST_LOGIN = "ADMIN_TEST_LOGIN";

    @NotNull
    public final static String ADMIN_TEST_PASSWORD = "ADMIN_TEST_PASSWORD";

    @NotNull
    public final static String ADMIN_TEST_EMAIL = "ADMIN_TEST@EMAIL";

    @NotNull
    public final static UserDTO USER_TEST = new UserDTO();

    @NotNull
    public final static UserDTO ADMIN_TEST = new UserDTO();

    @Nullable
    public final static UserDTO NULL_USER = null;

    @NotNull
    public final static String NON_EXISTING_USER_ID = UUID.randomUUID().toString();

    @NotNull
    public final static List<UserDTO> USER_LIST = Arrays.asList(USER_TEST, ADMIN_TEST);

    static {
        @NotNull final Properties properties = new Properties();
        try {
            properties.load(ClassLoader.getSystemResourceAsStream("application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        @NotNull final String passwordSecret = properties.getProperty("password.secret");
        @NotNull final Integer passwordIteration = Integer.parseInt(properties.getProperty("password.iteration"));
        USER_TEST.setLogin(USER_TEST_LOGIN);
        USER_TEST.setPasswordHash(HashUtil.salt(USER_TEST_PASSWORD, passwordSecret, passwordIteration));
        USER_TEST.setEmail(USER_TEST_EMAIL);
        ADMIN_TEST.setLogin(ADMIN_TEST_LOGIN);
        ADMIN_TEST.setPasswordHash(HashUtil.salt(ADMIN_TEST_PASSWORD, passwordSecret, passwordIteration));
        ADMIN_TEST.setEmail(ADMIN_TEST_EMAIL);
    }

}
